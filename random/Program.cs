﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace RandomN
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int scelta;
                Random random = new Random();
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Title = "Nomi Random Classe 4°B";

                Console.WriteLine("");
                Console.WriteLine(Properties.Resources.Title);
                Console.SetCursorPosition(50, 6);
                string userName = Environment.UserName;
                Console.WriteLine(userName);
                Console.SetCursorPosition(50, 7);
                Console.WriteLine("v1.1");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("");
                Console.WriteLine("Scegliere l'opzione:");
                Console.WriteLine("[1] - Possono uscire gli stessi nomi");
                Console.WriteLine("[2] - NON possono uscire gli stessi nomi");
                Console.WriteLine("[3] - In base all'ora");
                Console.WriteLine("[4] - In base al giorno");
                Console.WriteLine("[5] - Persone per gruppo");
                Console.WriteLine("[6] - Elenco nomi");


                ConsoleKey f = Console.ReadKey(true).Key;
                if (f == ConsoleKey.Escape)
                {
                    Console.WriteLine("Chiusura...");
                    Thread.Sleep(300);
                    Environment.Exit(0);
                }

                while (!Int32.TryParse(f.ToString().Remove(0, f.ToString().Length - 1), out scelta) || scelta <= 0 || scelta >= 7)
                {
                    Console.WriteLine("Premere un tasto valido!");
                    f = Console.ReadKey(true).Key;
                }


                switch (scelta)
                {
                    case 1:
                        {
                            Console.Clear();
                            Console.WriteLine("Premere invio per generare un nome o ESC per uscire");
                            while (f != ConsoleKey.Escape)
                            {
                                f = Console.ReadKey(true).Key;
                                int n = random.Next(0, TuttiNomi.Length);
                                Console.WriteLine(">>>> {0}", TuttiNomi[n]);

                            }
                        }
                        break;
                    case 2:
                        {
                            Console.Clear();
                            Console.WriteLine("Premere invio per generare un nome o ESC per uscire");

                            List<string> NomiDaEstrarre = new List<string>();
                            NomiDaEstrarre.AddRange(TuttiNomi.ToList());

                            while (NomiDaEstrarre.Count > 0 && f != ConsoleKey.Escape)
                            {
                                f = Console.ReadKey(true).Key;
                                int n = random.Next(NomiDaEstrarre.Count);
                                Console.WriteLine(">>>> {0}", NomiDaEstrarre[n]);
                                NomiDaEstrarre.RemoveAt(n);
                            }

                            Aspetta();

                        }
                        break;


                    case 3:
                        {
                            Console.Clear();
                            int mm = DateTime.Now.Minute;

                            if (mm == 0)
                                mm = DateTime.Now.Hour;

                            int n = SommaNumero(mm);
                            Console.WriteLine(">>>> {0}", TuttiNomi[n - 1]);

                            Aspetta();

                        }
                        break;

                    case 4:
                        {
                            Console.Clear();
                            int mm = DateTime.Now.Day;

                            int n = SommaNumero(mm);
                            Console.WriteLine(">>>> {0}", TuttiNomi[n - 1]);

                            Aspetta();
                        }
                        break;


                    case 5:
                        {
                            int personepergruppo;
                            int i = 0;
                            int gruppo = 0;
                            StringBuilder filetx = new StringBuilder();
                            Console.Clear();
                            Console.WriteLine("Inserisci le persone per gruppo");
                            while (!Int32.TryParse(Console.ReadLine(), out personepergruppo) || personepergruppo <= 0)
                            {
                                Console.WriteLine("Inserisci un numero corretto!");
                            }

                            List<string> NomiDaEstrarre = new List<string>();
                            NomiDaEstrarre.AddRange(TuttiNomi.ToList());

                            while (NomiDaEstrarre.Count > 0)
                            {
                                int n = random.Next(NomiDaEstrarre.Count);
                                if (i == 0 || i == personepergruppo)
                                {
                                    i = 0;
                                    gruppo++;
                                    Console.WriteLine("");
                                    Console.WriteLine(">>>>>>> Gruppo {0}:", gruppo);
                                    filetx.Append(Environment.NewLine + Environment.NewLine + ">>>>>>> Gruppo " + gruppo + ":");
                                }

                                i++;

                                Console.WriteLine(">>>> {0}", NomiDaEstrarre[n]);
                                filetx.Append(Environment.NewLine + ">>>> " + NomiDaEstrarre[n]);
                                NomiDaEstrarre.RemoveAt(n);
                            }

                            SalvaFile(filetx.ToString());
                        }

                        break;

                    case 6:
                        {
                            Console.Clear();
                            for (int i = 0; i < TuttiNomi.Length; i++)
                            {
                                if (i < 9) Console.Write(" ");
                                Console.WriteLine("{0}. {1}", i + 1, TuttiNomi[i]);
                            }

                            Aspetta();
                        }
                        break;

                }
                Console.WriteLine("");
                Console.Clear();

            }
        }

        static string[] TuttiNomi = { "Barigelli Alessio", "Barigelli Simone", "Boschetti Lorenzo", "Caputo Francesco", "Carloni Claudio",
             "Casarola Christian", "Chiappa Christian", "Luconi Simone","Marcantoni Enea", "Mazzanti Davide", "Moscatelli Dennis",
             "Paolucci Riccardo","Piagnerelli Gianmarco", "Pinto Matteo","Santinelli Riccardo", "Sulis Michele" };

        static void SalvaFile(string testo)
        {
            string DesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string FileName = "\\Gruppi_" + DateTime.Now.ToString("ddMMyy_HHmmss");
            string Ext = ".txt";
            testo = "Gruppi del " + DateTime.Now.ToString("dd/MM/yy") + " ore " + DateTime.Now.ToString("HH:mm:ss") + testo;
            File.WriteAllText(@DesktopPath + FileName + Ext, testo);
            Console.WriteLine("");
            Console.WriteLine("Gruppi salvati nel percorso: {0}", DesktopPath + FileName + Ext);
            Console.WriteLine("Premere invio per aprire il file o un tasto qualsiasi per tornare al menù");
            ConsoleKey f = Console.ReadKey(true).Key;
            if (f == ConsoleKey.Enter)
                Process.Start(@DesktopPath + FileName + Ext);
        }

        static void Aspetta()
        {
            Console.WriteLine("Premere un tasto per ritornare al menù");
            Console.ReadLine();
        }

        static int SommaNumero(int mm)
        {
            string smm = mm.ToString();

            if (smm.Length == 2)
            {
                int[] v = new int[2];

                v[0] = Convert.ToInt16(smm.ToCharArray()[0].ToString());
                v[1] = Convert.ToInt16(smm.ToCharArray()[1].ToString());

                int NumeroEstratto = 0;
                foreach (int n in v)
                {
                    NumeroEstratto += n;
                }

                if (NumeroEstratto < TuttiNomi.Length)
                {
                    return NumeroEstratto;
                }
                else
                {
                    smm = NumeroEstratto.ToString();
                    v = new int[2];
                    NumeroEstratto = 0;
                    v[0] = Convert.ToInt16(smm.ToCharArray()[0].ToString());
                    v[1] = Convert.ToInt16(smm.ToCharArray()[1].ToString());

                    foreach (int n in v)
                    {
                        NumeroEstratto += n;
                    }
                    return NumeroEstratto;
                }
            }
            else return Convert.ToInt16(smm.ToCharArray()[0].ToString());
        }
    }
}

