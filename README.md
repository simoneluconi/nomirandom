# Nomi Random
Questo programma estrae nomi in modo Random dall'elenco degli alunni della classe 4B.

## Funzionalità
#### Possono uscire gli stessi nomi 
I nomi vengono estratti in modo Random, senza tenere conto se il nome estratto è già stato estratto in precedenza.

#### NON possono uscire gli stessi nomi 
I nomi vengono estratti in modo Random, tenendo conto se il nome estratto è già stato estratto in precedenza, in tal caso, viene estratto un nuovo nome.
#### In base all'ora 
Il nome viene selezionato in base all'ora. Ad esempio se sono le **12:24** il numero estratto sarà il **6** (2+4), mentre se sono le **12:00** il numero estratto sarà il **3** (1+2), in quanto non può essere considerato il numero 0. Se il numero estratto supera il numero degli alunni il numero riviene sommato a sua volta.
#### In base al giorno
Il nome viene selezionato in base al giorno. Ad esempio oggi è il **13 Febbraio 2017** il numero estratto sarà il **4** (1+3). Se il numero estratto supera il numero degli alunni il numero riviene sommato a sua volta.
#### Persone per gruppo
Inserendo il numero di persone per gruppo vengono generati automaticamente dei gruppi di alunni.
#### Elenco nomi
Viene visualizzato l'elenco degli alunni, affiancati dalla loro posizione nell'elenco

## Codice sorgente
#### [Program.cs] (https://gitlab.com/simoneluconi/nomirandom/blob/master/random/Program.cs)

## Download
#### [setup.exe](http://simoneluconi.altervista.org/random/setup.exe)